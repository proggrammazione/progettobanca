import java.util.ArrayList;

class ContoCorrente {
    private Persona titolare;
    private double saldo;
    private boolean bloccato;
    private boolean fido;
    private ArrayList<Movimento> estrattoConto;

    public ContoCorrente(Persona titolare, double saldoIniz) {
        this.titolare = titolare;
        saldo = saldoIniz;
        bloccato = false;
        fido = false;
        estrattoConto = new ArrayList<>();
    }

    public ContoCorrente(String nome, String cognome, String cf, double saldoIniz) {
        this.titolare = new Persona(nome, cognome, cf);
        saldo = saldoIniz;
        bloccato = false;
        fido = false;
        estrattoConto = new ArrayList<>();
    }

    public double getSaldo() {
        return saldo;
    }

    public void bloccaConto() {
        bloccato = true;
    }

    public void sbloccaConto() {
        bloccato = false;
    }

    public boolean isBloccato() {
        return bloccato;
    }

    public boolean isFido() {
        return fido;
    }

    public void attivaFido() {
        fido = true;
    }

    public void disattivaFido() {
        fido = false;
    }

    public void bonifico(Movimento operazione) throws Exception {
        if (bloccato) throw new Exception("Impossibile eseguire operazioni su conto bloccato.");
        if (operazione.getImporto() < 0) throw new Exception("Non si possono eseguire addebiti attraverso i bonifici.");
        saldo += operazione.getImporto();
        estrattoConto.add(operazione);
    }

    public void prelievo(Movimento operazione) throws Exception {
        if (bloccato) throw new Exception("Impossibile eseguire operazioni su conto bloccato.");
        if (!fido && operazione.getImporto() > saldo)
            throw new Exception("Saldo insufficiente e fido non attivo.");
        saldo -= operazione.getImporto();
        estrattoConto.add(operazione);
    }

}
