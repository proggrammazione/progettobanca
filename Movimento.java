import java.time.LocalDate;

public class Movimento {
    private LocalDate dataRichiesta, dataValuta;
    private final String causale, tipologia;
    private final double importo;

    public Movimento(LocalDate dataRichiesta, LocalDate dataValuta, String causale, String tipologia, double importo) {
        this.dataRichiesta = dataRichiesta;
        this.dataValuta = dataValuta;
        this.causale = causale;
        this.tipologia = tipologia;
        this.importo = importo;
    }

    public LocalDate getDataRichiesta() {
        return dataRichiesta;
    }

    public LocalDate getDataValuta() {
        return dataValuta;
    }

    public String getCausale() {
        return causale;
    }

    public String getTipologia() {
        return tipologia;
    }

    public double getImporto() {
        return importo;
    }

    public String toString() {
        return "Movimento di " + importo + " del " + dataValuta;
    }

}

